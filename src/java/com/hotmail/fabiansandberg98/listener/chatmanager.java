package com.hotmail.fabiansandberg98.listener;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;

import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;

import com.hotmail.fabiansandberg98.entercolor;

public class chatmanager implements Listener {

	entercolor plugin;

	public chatmanager(entercolor plugin) {
		this.plugin = plugin;
	}

	PermissionUser user;

	@EventHandler
	public void onPlayerChat(AsyncPlayerChatEvent event) {
		Player player = event.getPlayer();

		if (plugin.getConfig().contains(
				"uuid." + player.getUniqueId() + ".chatcolor")) {
			String color = plugin.getConfig().getString(
					"uuid." + player.getUniqueId() + ".chatcolor");
			event.setMessage(plugin.replaceColors(color + event.getMessage()));
		} else {
			event.setMessage(event.getMessage());
		}

		if (plugin.chatmanager) {
			event.setFormat(player.getDisplayName() + ": " + event.getMessage());
		}
	}

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		this.user = PermissionsEx.getUser(player);
		String prefix = this.user.getPrefix(player.getWorld().getName());
		if (plugin.getConfig().contains(
				"uuid." + player.getUniqueId() + ".namecolor")) {
			String color = plugin.getConfig().getString(
					"uuid." + player.getUniqueId() + ".namecolor");
			player.setDisplayName(plugin.replaceColors(prefix + color
					+ player.getName())
					+ ChatColor.WHITE);
		}
	}
}
