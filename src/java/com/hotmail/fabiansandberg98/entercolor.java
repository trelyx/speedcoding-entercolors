package com.hotmail.fabiansandberg98;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import com.hotmail.fabiansandberg98.commands.chatcolor;
import com.hotmail.fabiansandberg98.commands.namecolor;
import com.hotmail.fabiansandberg98.listener.chatmanager;

public class entercolor extends JavaPlugin {

	public boolean chatmanager;

	@Override
	public void onEnable() {
		getLogger().info("Starting onEnable!  - EnterColor");
		if (Bukkit.getServer().getPluginManager()
				.isPluginEnabled("PermissionsEx")) {
			saveDefaultConfig();
			this.chatmanager = getConfig().getBoolean("chatmanager");
			this.getCommands();
			this.getListeners();
			getLogger().info("EnterColor has been enabled!  - EnterColor");
		} else {
			getLogger()
					.info("You have to install PermissionsEx in order to use this plugin!  - EnterColor");
			this.onDisable();
		}
	}

	@Override
	public void onDisable() {
		getLogger().info("Starting onDisable!  - EnterColor");
		getLogger().info("EnterColor has been disabled!  - EnterColor");
	}

	public void getCommands() {

		// Command "namecolor"
		namecolor namecolor = new namecolor(this);
		getCommand("namecolor").setExecutor(namecolor);

		// Command "chatcolor"
		chatcolor chatcolor = new chatcolor(this);
		getCommand("chatcolor").setExecutor(chatcolor);
	}

	public void getListeners() {
		chatmanager chatmanager = new chatmanager(this);
		this.getServer().getPluginManager().registerEvents(chatmanager, this);
	}

	public String replaceColors(String string) {
		return string.replaceAll("(&([a-f1-9]))", "\u00A7$2");
	}

}
