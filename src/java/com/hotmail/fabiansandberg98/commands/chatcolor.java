package com.hotmail.fabiansandberg98.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.hotmail.fabiansandberg98.entercolor;

public class chatcolor implements CommandExecutor {

	entercolor plugin;

	public chatcolor(entercolor plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command,
			String label, String[] args) {

		if (sender instanceof Player) {

			Player player = (Player) sender;
			if (player.hasPermission("chatcolor.use")) {

				if (args.length == 0) {
					player.sendMessage(ChatColor.DARK_RED
							+ "Too short arguments!");

				} else if (args.length == 1) {

					if (args[0].equalsIgnoreCase("darkblue")) {
						player.sendMessage(ChatColor.DARK_GREEN
								+ "You changed your chat color too "
								+ args[0].toLowerCase() + ".");
						plugin.getConfig().set(
								"uuid." + player.getUniqueId() + ".chatcolor",
								"&1");
						plugin.saveConfig();

					} else if (args[0].equalsIgnoreCase("darkgreen")) {
						player.sendMessage(ChatColor.DARK_GREEN
								+ "You changed your chat color too "
								+ args[0].toLowerCase() + ".");
						plugin.getConfig().set(
								"uuid." + player.getUniqueId() + ".chatcolor",
								"&2");
						plugin.saveConfig();

					} else if (args[0].equalsIgnoreCase("darkaqua")) {
						player.sendMessage(ChatColor.DARK_GREEN
								+ "You changed your chat color too "
								+ args[0].toLowerCase() + ".");
						plugin.getConfig().set(
								"uuid." + player.getUniqueId() + ".chatcolor",
								"&3");
						plugin.saveConfig();

					} else if (args[0].equalsIgnoreCase("darkred")) {
						player.sendMessage(ChatColor.DARK_GREEN
								+ "You changed your chat color too "
								+ args[0].toLowerCase() + ".");
						plugin.getConfig().set(
								"uuid." + player.getUniqueId() + ".chatcolor",
								"&4");
						plugin.saveConfig();

					} else if (args[0].equalsIgnoreCase("darkpurple")) {
						player.sendMessage(ChatColor.DARK_GREEN
								+ "You changed your chat color too "
								+ args[0].toLowerCase() + ".");
						plugin.getConfig().set(
								"uuid." + player.getUniqueId() + ".chatcolor",
								"&5");
						plugin.saveConfig();

					} else if (args[0].equalsIgnoreCase("gold")) {
						player.sendMessage(ChatColor.DARK_GREEN
								+ "You changed your chat color too "
								+ args[0].toLowerCase() + ".");
						plugin.getConfig().set(
								"uuid." + player.getUniqueId() + ".chatcolor",
								"&6");
						plugin.saveConfig();

					} else if (args[0].equalsIgnoreCase("gray")) {
						player.sendMessage(ChatColor.DARK_GREEN
								+ "You changed your chat color too "
								+ args[0].toLowerCase() + ".");
						plugin.getConfig().set(
								"uuid." + player.getUniqueId() + ".chatcolor",
								"&7");
						plugin.saveConfig();

					} else if (args[0].equalsIgnoreCase("dark_gray")) {
						player.sendMessage(ChatColor.DARK_GREEN
								+ "You changed your chat color too "
								+ args[0].toLowerCase() + ".");
						plugin.getConfig().set(
								"uuid." + player.getUniqueId() + ".chatcolor",
								"&8");
						plugin.saveConfig();

					} else if (args[0].equalsIgnoreCase("blue")) {
						player.sendMessage(ChatColor.DARK_GREEN
								+ "You changed your chat color too "
								+ args[0].toLowerCase() + ".");
						plugin.getConfig().set(
								"uuid." + player.getUniqueId() + ".chatcolor",
								"&9");
						plugin.saveConfig();

					} else if (args[0].equalsIgnoreCase("green")) {
						player.sendMessage(ChatColor.DARK_GREEN
								+ "You changed your chat color too "
								+ args[0].toLowerCase() + ".");
						plugin.getConfig().set(
								"uuid." + player.getUniqueId() + ".chatcolor",
								"&a");
						plugin.saveConfig();

					} else if (args[0].equalsIgnoreCase("aqua")) {
						player.sendMessage(ChatColor.DARK_GREEN
								+ "You changed your chat color too "
								+ args[0].toLowerCase() + ".");
						plugin.getConfig().set(
								"uuid." + player.getUniqueId() + ".chatcolor",
								"&b");
						plugin.saveConfig();

					} else if (args[0].equalsIgnoreCase("red")) {
						player.sendMessage(ChatColor.DARK_GREEN
								+ "You changed your chat color too "
								+ args[0].toLowerCase() + ".");
						plugin.getConfig().set(
								"uuid." + player.getUniqueId() + ".chatcolor",
								"&c");
						plugin.saveConfig();

					} else if (args[0].equalsIgnoreCase("lightpurple")) {
						player.sendMessage(ChatColor.DARK_GREEN
								+ "You changed your chat color too "
								+ args[0].toLowerCase() + ".");
						plugin.getConfig().set(
								"uuid." + player.getUniqueId() + ".chatcolor",
								"&d");
						plugin.saveConfig();

					} else if (args[0].equalsIgnoreCase("yellow")) {
						player.sendMessage(ChatColor.DARK_GREEN
								+ "You changed your chat color too "
								+ args[0].toLowerCase() + ".");
						plugin.getConfig().set(
								"uuid." + player.getUniqueId() + ".chatcolor",
								"&e");
						plugin.saveConfig();

					} else if (args[0].equalsIgnoreCase("white")) {
						player.sendMessage(ChatColor.DARK_GREEN
								+ "You changed your chat color too "
								+ args[0].toLowerCase() + ".");
						plugin.getConfig().set(
								"uuid." + player.getUniqueId() + ".chatcolor",
								"&f");
						plugin.saveConfig();

					} else if (args[0].equalsIgnoreCase("reset")) {
						player.sendMessage(ChatColor.DARK_GREEN
								+ "You removed your chat color.");
						plugin.getConfig().set(
								"uuid." + player.getUniqueId() + ".chatcolor",
								null);
						plugin.saveConfig();

					} else if (args[0].equalsIgnoreCase("list")) {
						player.sendMessage(ChatColor.DARK_GREEN
								+ "[Colors list]");
						player.sendMessage(ChatColor.DARK_BLUE + "darkblue");
						player.sendMessage(ChatColor.DARK_GREEN + "darkgreen");
						player.sendMessage(ChatColor.DARK_AQUA + "darkaqua");
						player.sendMessage(ChatColor.DARK_RED + "darkred");
						player.sendMessage(ChatColor.DARK_PURPLE + "darkpurple");
						player.sendMessage(ChatColor.GOLD + "gold");
						player.sendMessage(ChatColor.GRAY + "gray");
						player.sendMessage(ChatColor.DARK_GRAY + "darkgray");
						player.sendMessage(ChatColor.BLUE + "blue");
						player.sendMessage(ChatColor.GREEN + "green");
						player.sendMessage(ChatColor.AQUA + "aqua");
						player.sendMessage(ChatColor.RED + "red");
						player.sendMessage(ChatColor.LIGHT_PURPLE
								+ "lightpurple");
						player.sendMessage(ChatColor.YELLOW + "yellow");
						player.sendMessage(ChatColor.WHITE + "white");
						player.sendMessage(ChatColor.RESET + "reset");
						player.sendMessage(ChatColor.DARK_GREEN
								+ "-------------");

					} else {
						player.sendMessage(ChatColor.DARK_RED
								+ "Unknown command!");
					}
				} else if (args.length > 1) {
					player.sendMessage(ChatColor.DARK_RED
							+ "Too many arguments!");
				}

			} else {
				player.sendMessage(ChatColor.DARK_RED
						+ "You do not have permission to use that command!");
			}
		} else {
			sender.sendMessage("Only a player can use this command!");
		}
		return false;
	}

}