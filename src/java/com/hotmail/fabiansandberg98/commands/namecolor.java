package com.hotmail.fabiansandberg98.commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;

import com.hotmail.fabiansandberg98.entercolor;

public class namecolor implements CommandExecutor {

	entercolor plugin;

	public namecolor(entercolor plugin) {
		this.plugin = plugin;
	}

	PermissionUser user;

	@Override
	public boolean onCommand(CommandSender sender, Command command,
			String label, String[] args) {

		if (sender instanceof Player) {

			Player player = (Player) sender;
			this.user = PermissionsEx.getUser(player);
			String prefix = this.user.getPrefix(player.getWorld().getName());
			String suffix = this.user.getSuffix(player.getWorld().getName());

			if (player.hasPermission("namecolor.use")) {

				if (args.length == 0) {
					player.sendMessage(ChatColor.DARK_RED
							+ "Too short arguments!");

				} else if (args.length == 1) {

					if (args[0].equalsIgnoreCase("darkblue")) {
						player.setDisplayName(plugin.replaceColors(prefix)
								+ ChatColor.DARK_BLUE + player.getName() + plugin.replaceColors(suffix)
								+ ChatColor.RESET);
						player.sendMessage(ChatColor.DARK_GREEN
								+ "You changed your name color too: "
								+ ChatColor.DARK_BLUE + player.getDisplayName()
								+ ChatColor.DARK_GREEN + ".");
						plugin.getConfig().set(
								"uuid." + player.getUniqueId() + ".namecolor",
								"&1");
						plugin.saveConfig();

					} else if (args[0].equalsIgnoreCase("darkgreen")) {
						player.setDisplayName(plugin.replaceColors(prefix)
								+ ChatColor.DARK_GREEN + player.getName() + plugin.replaceColors(suffix)
								+ ChatColor.RESET);
						player.sendMessage(ChatColor.DARK_GREEN
								+ "You changed your name color too: "
								+ ChatColor.DARK_GREEN
								+ player.getDisplayName()
								+ ChatColor.DARK_GREEN + ".");
						plugin.getConfig().set(
								"uuid." + player.getUniqueId() + ".namecolor",
								"&2");
						plugin.saveConfig();

					} else if (args[0].equalsIgnoreCase("darkaqua")) {
						player.setDisplayName(plugin.replaceColors(prefix)
								+ ChatColor.DARK_AQUA + player.getName() + plugin.replaceColors(suffix)
								+ ChatColor.RESET);
						player.sendMessage(ChatColor.DARK_GREEN
								+ "You changed your name color too: "
								+ ChatColor.DARK_AQUA + player.getDisplayName()
								+ ChatColor.DARK_GREEN + ".");
						plugin.getConfig().set(
								"uuid." + player.getUniqueId() + ".namecolor",
								"&3");
						plugin.saveConfig();

					} else if (args[0].equalsIgnoreCase("darkred")) {
						player.setDisplayName(plugin.replaceColors(prefix)
								+ ChatColor.DARK_RED + player.getName() + plugin.replaceColors(suffix)
								+ ChatColor.RESET);
						player.sendMessage(ChatColor.DARK_GREEN
								+ "You changed your name color too: "
								+ ChatColor.DARK_RED + player.getDisplayName()
								+ ChatColor.DARK_GREEN + ".");
						plugin.getConfig().set(
								"uuid." + player.getUniqueId() + ".namecolor",
								"&4");
						plugin.saveConfig();

					} else if (args[0].equalsIgnoreCase("darkpurple")) {
						player.setDisplayName(plugin.replaceColors(prefix)
								+ ChatColor.DARK_BLUE + player.getName() + plugin.replaceColors(suffix)
								+ ChatColor.RESET);
						player.sendMessage(ChatColor.DARK_GREEN
								+ "You changed your name color too: "
								+ ChatColor.DARK_PURPLE
								+ player.getDisplayName()
								+ ChatColor.DARK_GREEN + ".");
						plugin.getConfig().set(
								"uuid." + player.getUniqueId() + ".namecolor",
								"&5");
						plugin.saveConfig();

					} else if (args[0].equalsIgnoreCase("gold")) {
						player.setDisplayName(plugin.replaceColors(prefix)
								+ ChatColor.GOLD + player.getName() + plugin.replaceColors(suffix)
								+ ChatColor.RESET);
						player.sendMessage(ChatColor.DARK_GREEN
								+ "You changed your name color too: "
								+ ChatColor.GOLD + player.getDisplayName()
								+ ChatColor.DARK_GREEN + ".");
						plugin.getConfig().set(
								"uuid." + player.getUniqueId() + ".namecolor",
								"&6");
						plugin.saveConfig();

					} else if (args[0].equalsIgnoreCase("gray")) {
						player.setDisplayName(plugin.replaceColors(prefix)
								+ ChatColor.GRAY + player.getName() + plugin.replaceColors(suffix)
								+ ChatColor.RESET);
						player.sendMessage(ChatColor.DARK_GREEN
								+ "You changed your name color too: "
								+ ChatColor.GRAY + player.getDisplayName()
								+ ChatColor.DARK_GREEN + ".");
						plugin.getConfig().set(
								"uuid." + player.getUniqueId() + ".namecolor",
								"&7");
						plugin.saveConfig();

					} else if (args[0].equalsIgnoreCase("darkgray")) {
						player.setDisplayName(plugin.replaceColors(prefix)
								+ ChatColor.DARK_GRAY + player.getName() + plugin.replaceColors(suffix)
								+ ChatColor.RESET);
						player.sendMessage(ChatColor.DARK_GREEN
								+ "You changed your name color too: "
								+ ChatColor.DARK_GRAY + player.getDisplayName()
								+ ChatColor.DARK_GREEN + ".");
						plugin.getConfig().set(
								"uuid." + player.getUniqueId() + ".namecolor",
								"&8");
						plugin.saveConfig();

					} else if (args[0].equalsIgnoreCase("blue")) {
						player.setDisplayName(plugin.replaceColors(prefix)
								+ ChatColor.BLUE + player.getName() + plugin.replaceColors(suffix)
								+ ChatColor.RESET);
						player.sendMessage(ChatColor.DARK_GREEN
								+ "You changed your name color too: "
								+ ChatColor.BLUE + player.getDisplayName()
								+ ChatColor.DARK_GREEN + ".");
						plugin.getConfig().set(
								"uuid." + player.getUniqueId() + ".namecolor",
								"&9");
						plugin.saveConfig();

					} else if (args[0].equalsIgnoreCase("green")) {
						player.setDisplayName(plugin.replaceColors(prefix)
								+ ChatColor.GREEN + player.getName() + plugin.replaceColors(suffix)
								+ ChatColor.RESET);
						player.sendMessage(ChatColor.DARK_GREEN
								+ "You changed your name color too: "
								+ ChatColor.GREEN + player.getDisplayName()
								+ ChatColor.DARK_GREEN + ".");
						plugin.getConfig().set(
								"uuid." + player.getUniqueId() + ".namecolor",
								"&a");
						plugin.saveConfig();

					} else if (args[0].equalsIgnoreCase("aqua")) {
						player.setDisplayName(plugin.replaceColors(prefix)
								+ ChatColor.AQUA + player.getName() + plugin.replaceColors(suffix)
								+ ChatColor.RESET);
						player.sendMessage(ChatColor.DARK_GREEN
								+ "You changed your name color too: "
								+ ChatColor.AQUA + player.getDisplayName()
								+ ChatColor.DARK_GREEN + ".");
						plugin.getConfig().set(
								"uuid." + player.getUniqueId() + ".namecolor",
								"&b");
						plugin.saveConfig();

					} else if (args[0].equalsIgnoreCase("red")) {
						player.setDisplayName(plugin.replaceColors(prefix)
								+ ChatColor.RED + player.getName() + plugin.replaceColors(suffix)
								+ ChatColor.RESET);
						player.sendMessage(ChatColor.DARK_GREEN
								+ "You changed your name color too: "
								+ ChatColor.RED + player.getDisplayName()
								+ ChatColor.DARK_GREEN + ".");
						plugin.getConfig().set(
								"uuid." + player.getUniqueId() + ".namecolor",
								"&c");
						plugin.saveConfig();

					} else if (args[0].equalsIgnoreCase("lightpurple")) {
						player.setDisplayName(plugin.replaceColors(prefix)
								+ ChatColor.LIGHT_PURPLE + player.getName() + plugin.replaceColors(suffix)
								+ ChatColor.RESET);
						player.sendMessage(ChatColor.DARK_GREEN
								+ "You changed your name color too: "
								+ ChatColor.LIGHT_PURPLE
								+ player.getDisplayName()
								+ ChatColor.DARK_GREEN + ".");
						plugin.getConfig().set(
								"uuid." + player.getUniqueId() + ".namecolor",
								"&d");
						plugin.saveConfig();

					} else if (args[0].equalsIgnoreCase("yellow")) {
						player.setDisplayName(plugin.replaceColors(prefix)
								+ ChatColor.YELLOW + player.getName() + plugin.replaceColors(suffix)
								+ ChatColor.RESET);
						player.sendMessage(ChatColor.DARK_GREEN
								+ "You changed your name color too: "
								+ ChatColor.YELLOW + player.getDisplayName()
								+ ChatColor.DARK_GREEN + ".");
						plugin.getConfig().set(
								"uuid." + player.getUniqueId() + ".namecolor",
								"&e");
						plugin.saveConfig();

					} else if (args[0].equalsIgnoreCase("white")) {
						player.setDisplayName(plugin.replaceColors(prefix)
								+ ChatColor.WHITE + player.getName() + plugin.replaceColors(suffix)
								+ ChatColor.RESET);
						player.sendMessage(ChatColor.DARK_GREEN
								+ "You changed your name color too: "
								+ ChatColor.WHITE + player.getDisplayName()
								+ ChatColor.DARK_GREEN + ".");
						plugin.getConfig().set(
								"uuid." + player.getUniqueId() + ".namecolor",
								"&f");
						plugin.saveConfig();

					} else if (args[0].equalsIgnoreCase("reset")) {
						player.setDisplayName(plugin.replaceColors(prefix)
								+ player.getName()  + plugin.replaceColors(suffix));
						player.sendMessage(ChatColor.DARK_GREEN
								+ "You removed your name color.");
						plugin.getConfig().set(
								"uuid." + player.getUniqueId() + ".namecolor",
								null);
						plugin.saveConfig();

					} else if (args[0].equalsIgnoreCase("list")) {
						player.sendMessage(ChatColor.DARK_GREEN
								+ "[Name colors list]");
						player.sendMessage(ChatColor.DARK_BLUE + "darkblue");
						player.sendMessage(ChatColor.DARK_GREEN + "darkgreen");
						player.sendMessage(ChatColor.DARK_AQUA + "darkaqua");
						player.sendMessage(ChatColor.DARK_RED + "darkred");
						player.sendMessage(ChatColor.DARK_PURPLE + "darkpurple");
						player.sendMessage(ChatColor.GOLD + "gold");
						player.sendMessage(ChatColor.GRAY + "gray");
						player.sendMessage(ChatColor.DARK_GRAY + "darkgray");
						player.sendMessage(ChatColor.BLUE + "blue");
						player.sendMessage(ChatColor.GREEN + "green");
						player.sendMessage(ChatColor.AQUA + "aqua");
						player.sendMessage(ChatColor.RED + "red");
						player.sendMessage(ChatColor.LIGHT_PURPLE
								+ "lightpurple");
						player.sendMessage(ChatColor.YELLOW + "yellow");
						player.sendMessage(ChatColor.WHITE + "white");
						player.sendMessage(ChatColor.RESET + "reset");
						player.sendMessage(ChatColor.DARK_GREEN
								+ "------------------");

					} else {
						player.sendMessage(ChatColor.DARK_RED
								+ "Unknown command!");
					}
				} else if (args.length > 1) {
					player.sendMessage(ChatColor.DARK_RED
							+ "Too many arguments!");
				}

			} else {
				player.sendMessage(ChatColor.DARK_RED
						+ "You do not have permission to use that command!");
			}
		} else {
			sender.sendMessage("Only a player can use this command!");
		}
		return false;
	}

}
