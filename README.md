# EnterColor#

This is a simple Name color and Chat color plugin! (It requires PermissionsEx!)
It also have a little Chat Manager. It will make the chat look like this:  AdobeGFX: Hi.
you can easily disable ChatManager in the config.yml by setting; chatmanager: false.

## **Commands:** ##
* * /namecolor <color> - *- Adds a color to your name!*
* * /namecolor list - *- This will give you a list of every color for /namecolor*
* * /chatcolor <color> - *- Adds a color yo your chat!*
* * /chatcolor list - *- This will give you a list of every color for /chatcolor*


## **Permissions:** ##
* * namecolor.use - *- Gives you access to /namecolor!*
* * chatcolor.use - *- Gives you access to /chatcolor*

## **Found a bug/error, or just want to talk?** ##
* * Skype: neeh98
* * Mail: fabiansandberg98@hotmail.com

## **Social links:** ##
* * Twitter: www.twitter.com/Trelyx
* * Portfolio: www.behance.net/Trelyx
* * Youtube: www.youtube.com/imTrelyx
* * Soundcloud: www.soundcloud.com/fabianauu